package com.example.menghitungluas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText editPanjang, editlebar;
    private Button btnHitung;
    private TextView txthasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Hitung Luas Persegi Panjang");

        editPanjang = findViewById(R.id.edit_panjang);
        editlebar = findViewById(R.id.edit_lebar);
        btnHitung = findViewById(R.id.btn_hitung);
        txthasil = findViewById(R.id.txt_luas);

        btnHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String panjang = editPanjang.getText().toString();
                String lebar = editlebar.getText().toString();

                double p = Double.parseDouble(panjang);
                double l = Double.parseDouble(lebar);

                double luas = p * l;

                txthasil.setText("luas = " + luas);
            }
        });
    }
}
